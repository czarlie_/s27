const http = require('http')
const { type } = require('os')
const port = 6969
const failMessage = `Are you a failure? lol 😇`

// mock database
// prettier-ignore
let directory = [
  {
    'name': 'John',
    'email': 'johnny@gmail.com'
  },
  {
    "name": 'Smithy',
    "email": 'smithysmoothie@gmail.com'
  }
]

const server = http.createServer((req, res) => {
  // get all users
  if (req.url === '/users' && req.method === 'GET') {
    res.writeHead(200, { 'Content-Type': 'text/plain' })
    res.write(JSON.stringify(directory))
    res.end()
  }
  // add new user
  if (req.url === '/users' && req.method === 'POST') {
    // res.writeHead(200, { 'Content-Type': 'text/plain' })
    // res.end(`Data to be sent to the database (post) 🥺`)
  }
  // else console.log(failMessage)

  let requestBody = ''

  req.on('data', (data) => {
    requestBody += data
  })

  req.on('end', () => {
    console.log(typeof requestBody)
    requestBody = JSON.parse(requestBody)
    console.log(requestBody)

    // prettier-ignore
    let newUser = {
      'name': requestBody.name,
      "email": requestBody.email
    }

    directory.push(newUser)
    console.log(directory)

    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.write(JSON.stringify(newUser))
    res.end()
  })
})

server.listen(port, 'localhost', () => {
  console.log(`Listening to port: ${port}`)
})
